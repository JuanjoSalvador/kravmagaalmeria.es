let gulp     = require('gulp');
let cleanCSS = require('gulp-clean-css');
let rename   = require('gulp-rename');
let htmlmin  = require('gulp-htmlmin');
 
gulp.task('minify-css', () => 
    gulp.src('src/css/*.css')
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('public/css/'))
);

gulp.task('minify-html', () =>
    gulp.src('src/**/*.html')
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('public/'))
);

gulp.task('assets', () => 
    gulp.src('assets/**')
        .pipe(gulp.dest('public/assets/'))
);

gulp.task('robots', () =>
    gulp.src('robots.txt')
        .pipe(gulp.dest('public/'))
);

gulp.task('sitemap', () =>
    gulp.src('sitemap.xml')
        .pipe(gulp.dest('public/'))
);

gulp.task('default', gulp.parallel('minify-css', 'minify-html', 'assets', 'robots', 'sitemap'))